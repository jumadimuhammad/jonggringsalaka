<?php

namespace App\Http\Controllers;

use App\Models\Tool;
use App\Models\ToolHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ToolController extends Controller
{

    public function index()
    {
        return view('tool.index', [
            'tools' => Tool::where('type', 'tool')->orderBy('created_at','DESC')->get(),
        ]);
    }

    public function create()
    {
        return view('tool.input');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'mark' => 'required',
            'code' => 'required|unique:tools',
            'name' => 'required',
            'information' => 'required',
            'category' => 'required',
            'condition' => 'required',
            'year' => 'required',
        ]);

        $id = Str::uuid()->toString();
        $validateData['id'] = $id;
        $validateData['created_by'] = $request->user;
        $validateData['type'] = 'tool';
        Tool::create($validateData);

        $validateData['id'] = Str::uuid()->toString();
        $validateData['tool_id'] = $id;
        $validateData['created_by'] = $request->user;
        $validateData['information'] = 'Di buat oleh user';

        ToolHistory::create($validateData);

        return redirect('/tools')->with('message', 'Data berhasil ditambahkan !');
    }

    public function show($id)
    {
        return view('tool.detail', [
            'tool' => Tool::find($id),
            'histories' => ToolHistory::where('tool_id', $id)->orderBy('created_at','DESC')->get(),
        ]);
    }

    public function edit($id)
    {
        return view('tool.edit', [
            'tool' => Tool::find($id),
        ]);
    }

    public function editChecklist($id)
    {
        return view('tool.checklist', [
            'tool' => Tool::find($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'mark' => 'required',
            'name' => 'required',
            'information' => 'required',
            'category' => 'required',
            'condition' => 'required',
            'year' => 'required',
        ];

        $tool = Tool::find($id);
        if ($tool->code != $request->code) {
            $rules['code'] = 'required|unique:tools';
        }

        $validateData = $request->validate($rules);
        $validateData['updated_by'] = $request->user;
        Tool::where('id', $id)->update($validateData);

        $validateData['id'] = Str::uuid()->toString();
        $validateData['tool_id'] = $id;
        $validateData['created_by'] = $request->user;
        $validateData['information'] = 'Di perbaharui oleh user';
        ToolHistory::create($validateData);

        return redirect('/tools')->with('message', 'Data berhasil diupdate !');
    }

    public function updateChecklist(Request $request, $id)
    {
        $validateData = $request->validate([
            'condition' => 'required',
        ]);
        $validateData['checklist_at'] = now();
        $validateData['updated_by'] = $request->user;
        Tool::where('id', $id)->update($validateData);

        $validateData['id'] = Str::uuid()->toString();
        $validateData['tool_id'] = $id;
        $validateData['created_by'] = $request->user;
        $validateData['information'] = 'Di check list oleh user';
        ToolHistory::create($validateData);

        return redirect('/tools')->with('message', 'Data berhasil di check list !');
    }

    public function destroy($id)
    {
        Tool::destroy($id);
        ToolHistory::where('tool_id', $id)->delete();

        return redirect('/tools')->with('message', 'Data berhasil dihapus !!');

    }
}
