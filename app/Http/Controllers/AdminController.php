<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        //
        return view('admin.index', [
            'user' => User::where('admin', true)->get(),
            'users' => User::where('admin', false)->get(),
        ]);
    }

    // public function show($id)
    // {
    //     User::where('id', $id)->update([
    //         'admin' => 1,
    //     ]);

    //     return redirect('/admins')->with('message', 'Admin berhasil ditambahkan !');
    // }

    public function edit($id)
    {
        return view('admin.edit', [
            'user' => User::find($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'access' => 'required',
        ]);

        User::where('id', $id)->update([
            'admin' => 1,
            'access' => $request->access,
        ]);

        return redirect('/admins')->with('message', 'Admin berhasil ditambahkan !');
    }

    public function destroy($id)
    {
        User::where('id', $id)->update([
            'admin' => 0,
            'access' => 'user',
        ]);

        return redirect('/admins')->with('message', 'Admin berhasil dihapus !');
    }
}
