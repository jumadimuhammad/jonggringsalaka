<?php

namespace App\Http\Controllers;

use App\Models\Finance;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FinanceController extends Controller
{
    public function index()
    {
        return view('finance/index', [
            'finance' => Finance::all(),
        ]);
    }

    public function create()
    {
        return view('finance/input');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'amount' => 'required|min:0',
            'transaction' => 'required',
            'description' => 'required',
            'user_id' => 'required',
        ]);

        $validateData['id'] = Str::uuid()->toString();

        Finance::create($validateData);
        return redirect('/finances')->with('message', 'Data berhasil ditambahkan !');

    }

    public function show($id)
    {
        return view('finance/detail', [
            'finance' => Finance::find($id),
        ]);

    }

    public function edit($id)
    {
        return view('finance/edit', [
            'finance' => Finance::find($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'amount' => 'required|min:0',
            'transaction' => 'required',
            'description' => 'required',
            'user_id' => 'required',
        ]);

        Finance::where('id', $id)->update($validateData);
        return redirect('/finances')->with('message', 'Data berhasil diupdate !');
    }

    public function destroy($id)
    {
        Finance::destroy($id);
        return redirect('/finances')->with('message', 'Data berhasil dihapus !!');
    }
}
