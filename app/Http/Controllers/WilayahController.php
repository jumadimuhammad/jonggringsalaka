<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;

class WilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function province()
    {
        $response = Http::get('https://sig.bps.go.id/rest-bridging/getwilayah?level=provinsi&parent=0');

        return response()->json([
            'provinces' => $response->object(),
        ]);
    }

    public function city($id)
    {
        $response = Http::get('https://sig.bps.go.id/rest-bridging/getwilayah?level=kabupaten&parent='.$id);

        return response()->json([
            'cities' => $response->object(),
        ]);
    }

    public function district($id)
    {
        $response = Http::get('https://sig.bps.go.id/rest-bridging/getwilayah?level=kecamatan&parent='.$id);

        return response()->json([
            'districts' => $response->object(),
        ]);
    }

    public function village($id)
    {
        $response = Http::get('https://sig.bps.go.id/rest-bridging/getwilayah?level=desa&parent='.$id);

        return response()->json([
            'villages' => $response->object(),
        ]);
    }
    
}
