<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index', [
            'users' => User::orderBy('created_at','DESC')->paginate(10),
        ]);
    }

    public function create()
    {
        return view('user.input');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'najs' => 'required|unique:users|min:6|max:6',
        ]);

        $validateData['id'] = Str::uuid()->toString();
        $validateData['admin'] = false;
        $validateData['password'] = Hash::make(Request()->najs);

        User::create($validateData);
        return redirect('/users')->with('message', 'Data berhasil ditambahkan !');

    }

    public function show($id)
    {
        return view('user.detail', [
            'user' => User::find($id),
        ]);
    }

    public function edit($id)
    {
        $response = Http::get('https://sig.bps.go.id/rest-bridging/getwilayah?level=provinsi&parent=0');

        //https://sig.bps.go.id/bridging-kode/index
        $province = $response->object();
        return view('user.edit', [
            'user' => User::find($id),
            'provinces'=> $province,
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'other_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'year' => 'required',
            'najs' => 'required|min:6|max:6',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'village' => 'required',

        ];

        $user = User::find($id);
        if ($user->email != $request->email) {
            $rules['email'] = 'required|unique:users|email';
        };
        
        $validateData = $request->validate($rules);
        User::where('id', $id)->update($validateData);

        return redirect('/users')->with('message', 'Data berhasil diupdate !');
    }

    public function change_password(Request $request, $id)
    {
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required',
        ];

        $validateData = $request->validate($rules);
        if ($validateData['new_password'] != $validateData['confirm_password']) {
            return redirect('/users/change-password')->with('message', 'Password baru tidak sama dengan confirm password!');
        }

        $user = User::find($id);

        if (!Hash::check($validateData['old_password'], $user->password)) {
            return redirect('/users/change-password')->with('message', 'Password lama tidak sesuai!');
        }
        
        $data['password'] = Hash::make($validateData['new_password']);
        User::where('id', $id)->update($data);

        return redirect('/logout')->with('message', 'Password berhasil diperbarui!');
    }

    public function destroy($id)
    {
        User::destroy($id);
        return redirect('/users')->with('message', 'Data berhasil dihapus !!');
    }
}
