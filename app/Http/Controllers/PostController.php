<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class PostController extends Controller
{
    public function index()
    {
        return view('post.index', [
            'posts' => Post::all(),
        ]);
    }

    public function create()
    {
        return view('post.input', [
            'categories' => Category::all(),
        ]);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'slug' => 'required|unique:posts',
            'category_id' => 'required',
            'body' => 'required',
        ]);

        Post::create($validateData);
        return redirect('/posts')->with('message', 'Data berhasil ditambahkan !');
    }

    public function show($id)
    {
        return view('post.detail', [
            'post' => Post::find($id),
        ]);
    }

    public function edit($id)
    {
        return view('post.edit', [
            'post' => Post::find($id),
            'categories' => Category::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'user_id' => 'required',
            'title' => 'required',
            'category_id' => 'required',
            'body' => 'required',
        ];

        $post = Post::find($id);
        if ($post->slug != $request->slug) {
            $rules['slug'] = 'required|unique:posts';
        }

        $validateData = $request->validate($rules);
        Post::where('slug', $id)->update($validateData);
        return redirect('/posts')->with('message', 'Data berhasil diupdate !');
    }

    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/posts')->with('message', 'Data berhasil dihapus !!');
    }

    public function createSlug(Request $request)
    {
        $slug = SlugService::createSlug(Post::class, 'slug', $request->title);
        return response()->json([
            'slug' => $slug,
        ]);
    }

}
