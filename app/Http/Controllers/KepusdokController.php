<?php

namespace App\Http\Controllers;

use App\Models\Tool;
use App\Models\Kepusdok;
use App\Models\ToolHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class KepusdokController extends Controller
{

    public function index()
    {
        return view('kepusdok.index', [
            'kepusdoks' => Kepusdok::all()->sortByDesc('created_at'),
        ]);
    }

    public function create()
    {
        return view('kepusdok.input');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'code' => 'required|unique:kepusdoks',
            'title' => 'required',
            'information' => 'required',
            'category' => 'required',
            'condition' => 'required',
            'year' => 'required',
            'shape' => 'required',
        ]);

        $id = Str::uuid()->toString();
        $validateData['id'] = $id;
        $validateData['created_by'] = $request->user;
        Kepusdok::create($validateData);

        $validateData['id'] = Str::uuid()->toString();
        $validateData['tool_id'] = $id;
        $validateData['created_by'] = $request->user;
        $validateData['information'] = 'Di buat oleh user';

        ToolHistory::create($validateData);

        return redirect('/kepusdoks')->with('message', 'Data berhasil ditambahkan !');
    }

    public function show($id)
    {
        return view('kepusdok.detail', [
            'kepusdok' => Kepusdok::find($id),
            'histories' => ToolHistory::where('tool_id', $id)->orderBy('created_at','DESC')->get(),
        ]);
    }

    public function edit($id)
    {
        return view('kepusdok.edit', [
            'kepusdok' => Kepusdok::find($id),
        ]);
    }

    public function editChecklist($id)
    {
        return view('kepusdok.checklist', [
            'kepusdok' => Kepusdok::find($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'information' => 'required',
            'category' => 'required',
            'condition' => 'required',
            'year' => 'required',
            'shape' => 'required',
        ];

        $kepusdok = Kepusdok::find($id);
        if ($kepusdok->code != $request->code) {
            $rules['code'] = 'required|unique:kepusdoks';
        }

        $validateData = $request->validate($rules);
        $validateData['updated_by'] = $request->user;
        Kepusdok::where('id', $id)->update($validateData);

        $validateData['id'] = Str::uuid()->toString();
        $validateData['tool_id'] = $id;
        $validateData['created_by'] = $request->user;
        $validateData['information'] = 'Di perbaharui oleh user';
        ToolHistory::create($validateData);

        return redirect('/kepusdoks')->with('message', 'Data berhasil diupdate !');
    }

    public function updateChecklist(Request $request, $id)
    {
        $validateData = $request->validate([
            'condition' => 'required',
        ]);
        $validateData['checklist_at'] = now();
        $validateData['updated_by'] = $request->user;
        Kepusdok::where('id', $id)->update($validateData);

        $validateData['id'] = Str::uuid()->toString();
        $validateData['tool_id'] = $id;
        $validateData['created_by'] = $request->user;
        $validateData['information'] = 'Di check list oleh user';
        ToolHistory::create($validateData);

        return redirect('/kepusdoks')->with('message', 'Data berhasil di check list !');
    }

    public function destroy($id)
    {
        Tool::destroy($id);
        ToolHistory::where('tool_id', $id)->delete();

        return redirect('/kepusdoks')->with('message', 'Data berhasil dihapus !!');

    }
}
