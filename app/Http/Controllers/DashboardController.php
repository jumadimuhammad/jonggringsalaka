<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard.index', [
            'posts' => Post::latest()->take(5)->get(),
            'count' =>User::All()->count(),
        ]);
    }

    public function show($id)
    {
        return view('dashboard.detail', [
            'post' => Post::find($id),
        ]);
    }
}
