<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ToolModel extends Model
{
    public function allTool()
    {
        return DB::table('tools')
            ->select('tools.*')
            ->selectRaw("(CASE WHEN (MONTH(checklist_at) = MONTH(now()) AND YEAR(checklist_at) = YEAR(now())) THEN 1 ELSE 0 END) as checklist")
            ->orderBy('created_at', 'desc')
            ->paginate(10);
    }

    public function searchTool($req)
    {
        return DB::table('tools')->where('name', 'like', '%' . $req . '%')
            ->orWhere('total', 'like', '%' . $req . '%')
            ->orWhere('made', 'like', '%' . $req . '%')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
    }

    public function detail($id)
    {
        return DB::table('tools')->where('id', $id)->first();
    }

    public function getHistories($id)
    {
        return DB::table('tool_histories')
            ->where('tool_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function createTool($data)
    {
        DB::table('tools')->insert($data);
    }

    public function updateTool($id_tool, $data)
    {
        Db::table('tools')
            ->where('id', $id_tool)
            ->update($data);
    }

    public function createHistories($data)
    {
        DB::table('tool_histories')->insert($data);
    }

    public function deleteTool($id_tool)
    {
        Db::table('tools')
            ->where('uuid', $id_tool)
            ->delete();
    }
}
