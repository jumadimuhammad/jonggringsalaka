<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory;
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'id',
        'name',
        'other_name',
        'address',
        'phone',
        'year',
        'email',
        'najs',
        'admin',
        'province',
        'city',
        'district',
        'village',
        'password',
    ];

    protected $hidden =['password'];
}
