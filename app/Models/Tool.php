<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    use HasFactory;
    protected $table = 'tools';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'id',
        'code',
        'category',
        'name',
        'year',
        'mark',
        'type',
        'information',
        'created_by',
        'updated_by',
        'checklist_at',
        'condition',
    ];
}
