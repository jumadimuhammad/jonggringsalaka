<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ToolHistory extends Model
{
    use HasFactory;
    protected $table = 'tool_histories';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'tool_id',
        'condition',
        'created_by',
        'information',
    ];
}
