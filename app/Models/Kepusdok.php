<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kepusdok extends Model
{
    use HasFactory;
    protected $table = 'kepusdoks';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'id',
        'code',
        'title',
        'year',
        'category',
        'information',
        'created_by',
        'updated_by',
        'checklist_at',
        'condition',
    ];
}
