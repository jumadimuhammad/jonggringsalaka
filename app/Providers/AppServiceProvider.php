<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        Gate::define('admin', function (User $user) {
            return $user->admin == true;
        });

        Gate::define('administrator', function (User $user) {
            return $user->access == 'administrator';
        });

        Gate::define('finance', function (User $user) {
            return $user->access == 'administrator' || $user->access == 'keuangan';
        });

        Gate::define('tool', function (User $user) {
            return $user->access == 'administrator' || $user->access == 'peralatan';
        });

        Gate::define('humas', function (User $user) {
            return $user->access == 'administrator' || $user->access == 'humas';
        });

        Gate::define('kepusdok', function (User $user) {
            return $user->access == 'administrator' || $user->access == 'kepusdok';
        });

        Gate::define('kesra', function (User $user) {
            return $user->access == 'administrator' || $user->access == 'kesra';
        });

    }
}
