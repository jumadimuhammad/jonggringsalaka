<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKepusdoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kepusdoks', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code', 15);
            $table->string('title', 100);
            $table->string('year', 5);
            $table->string('category', 20);
            $table->string('information')->nullable();
            $table->string('created_by', 10)->nullable();
            $table->string('updated_by', 10)->nullable();
            $table->timestamp('checklist_at')->nullable();
            $table->enum('condition', ['Baik', 'Rusak ringan', 'Rusak berat', 'Hilang']);
            $table->enum('shape', ['Fisik', 'Digital', 'Fisik & digital']);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kepusdoks');
    }
}
