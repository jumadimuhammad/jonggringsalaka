<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ToolController;
use App\Http\Controllers\KepusdokController;
use App\Http\Controllers\KesraController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WilayahController;
use Illuminate\Support\Facades\Route;

//view
Route::view('/users/profile', 'user.profile')->middleware('auth');
Route::view('/users/change-password', 'user.change_password')->middleware('auth');
Route::view('/not-found', 'not_found')->middleware('auth');

//wilayah
Route::get('/provinces', [WilayahController::class, 'province']);
Route::get('/cities/{id}', [WilayahController::class, 'city']);
Route::get('/districts/{id}', [WilayahController::class, 'district']);
Route::get('/villages/{id}', [WilayahController::class, 'village']);


//Login
Route::get('/login', [AuthenticateController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [AuthenticateController::class, 'authenticate'])->middleware('guest');
Route::get('/logout', [AuthenticateController::class, 'logout'])->middleware('auth');

// dashboard
Route::get('/', [DashboardController::class, 'index'])->middleware('auth');
Route::get('/dashboard/{id}', [DashboardController::class, 'show'])->middleware('auth');

// Route users
Route::resource('users', 'UserController')->middleware('auth');
Route::put('/users/{id}/change-password', [UserController::class, 'change_password'])->middleware('auth');

// Route finances
Route::resource('finances', FinanceController::class)->middleware('auth');

// Route tools
Route::resource('tools', 'ToolController')->middleware('auth');
Route::get('/tools/{id}/checks', [ToolController::class, 'editChecklist'])->middleware('auth');
Route::put('/tools/{id}/checks', [ToolController::class, 'updateChecklist'])->middleware('auth');

// Route kesras
Route::resource('kesras', 'KesraController')->middleware('auth');
Route::get('/kesras/{id}/checks', [KesraController::class, 'editChecklist'])->middleware('auth');
Route::put('/kesras/{id}/checks', [KesraController::class, 'updateChecklist'])->middleware('auth');

// Route kepusdoks
Route::resource('kepusdoks', 'KepusdokController')->middleware('auth');
Route::get('/kepusdoks/{id}/checks', [KepusdokController::class, 'editChecklist'])->middleware('auth');
Route::put('/kepusdoks/{id}/checks', [KepusdokController::class, 'updateChecklist'])->middleware('auth');

// Route posts
Route::get('/posts/check-slug', [PostController::class, 'createSlug'])->middleware('admin');
Route::resource('posts', 'PostController')->parameters([
    'posts' => 'posts:slug',
])->middleware('admin');

// Route categories
Route::resource('categories', CategoryController::class)->middleware('admin');

// Route admins
Route::resource('admins', AdminController::class)->middleware('admin');


