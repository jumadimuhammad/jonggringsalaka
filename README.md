<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


```
     __                                   .__                                .__          __            
    |__| ____   ____    ____   ___________|__| ____    ____     ___________  |  | _____  |  | _______   
    |  |/  _ \ /    \  / ___\ / ___\_  __ \  |/    \  / ___\   /  ___/\__  \ |  | \__  \ |  |/ /\__  \  
    |  (  <_> )   |  \/ /_/  > /_/  >  | \/  |   |  \/ /_/  >  \___ \  / __ \|  |__/ __ \|    <  / __ \_
/\__|  |\____/|___|  /\___  /\___  /|__|  |__|___|  /\___  /  /____  >(____  /____(____  /__|_ \(____  /
\______|           \//_____//_____/               \//_____/        \/      \/          \/     \/     \/ 

```



## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
