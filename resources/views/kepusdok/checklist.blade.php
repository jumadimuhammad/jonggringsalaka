@extends('template.index')
@section('page_title', 'Alat')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tooltips <small>Hover to view</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">

                        {!!
                        Form::open(['url'=>'/kepusdoks/'.$kepusdok->id.'/checks','method'=>'PUT','enctype'=>'multipart/form-data'])
                        !!}

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">User</label>
                            <input type="text" name="user" class="form-control" value="{{Auth::user()->najs}}" readonly>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kode</label>
                            <input type="text" name="code" class="form-control" value="{{$kepusdok->code}}" readonly>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Judul</label>
                            <input type="text" name="title" class="form-control" value="{{$kepusdok->title}}" readonly>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Keterangan</label>
                            <input type="text" name="information" class="form-control" value="{{$kepusdok->information}}"
                                readonly>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kategori</label>
                            <input type="text" name="category" class="form-control" value="{{$kepusdok->category}}" readonly>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Bentuk</label>
                            <select id="" name="shape" class="form-control" required readonly>
                                <option value="{{$kepusdok->shape}}">{{$kepusdok->shape}}</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kondisi</label>
                            <select id="" name="condition" class="form-control" required>
                                <option value="{{$kepusdok->condition}}">{{$kepusdok->condition}}</option>
                                <option value="Baik">Baik</option>
                                <option value="Rusak ringan">Rusak ringan</option>
                                <option value="Rusak berat">Rusak berat</option>
                                <option value="Hilang">Hilang</option>
                            </select>
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection