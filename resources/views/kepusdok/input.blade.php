@extends('template.index')
@section('page_title', 'Alat')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tooltips <small>Hover to view</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">

                        {!!
                        Form::open(['url'=>'/kepusdoks','method'=>'POST','enctype'=>'multipart/form-data'])
                        !!}

                        <input type="text" name="user" value="{{Auth::user()->najs}}" hidden>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kode</label>
                            <input type="text" name="code" class="form-control" required>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Judul</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Tahun pengadaan</label>
                            <input type="text" name="year" class="form-control" placeholder="2021" required>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kategori</label>
                            <input type="text" name="category" class="form-control" placeholder="" required>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Bentuk</label>
                            <select id="" name="shape" class="form-control" required>
                                <option value="Fisik">Fisik</option>
                                <option value="Digital">Digital</option>
                                <option value="Fisik & digital">Fisik & Digital</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kondisi</label>
                            <select id="" name="condition" class="form-control" required>
                                <option value="Baik">Baik</option>
                                <option value="Rusak">Rusak ringan</option>
                                <option value="Rusak">Rusak berat</option>
                                <option value="Hilang">Hilang</option>
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Keterangan</label>
                            <input type="text" name="information" class="form-control">
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection