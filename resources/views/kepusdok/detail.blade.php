@extends('template.index')
@section('page_title', 'Peralatan')
@section('sub_page_title', 'Some examples to get you started')

@section('content')

<div class="row">
    <div class="col-md-4 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail alat <small>Weekly progress</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Code</th>
                                <td><b>{{$kepusdok->code}}</b></td>
                            </tr>
                            <tr>
                                <th>Judul</th>
                                <td><b>{{$kepusdok->title}}</b></td>
                            </tr>
                            <tr>
                                <th>Kategori</th>
                                <td>{{$kepusdok->category}}</td>
                            </tr>
                            <tr>
                                <th>Tahun pengadaan</th>
                                <td>{{$kepusdok->year}}</td>
                            </tr>
                            <tr>
                                <th>Kondisi</th>
                                <td>
                                    @if ($kepusdok->condition == 'Baik')
                                    <span class="label label-success">{{$kepusdok->condition}}</span>
                                    @elseif ($kepusdok->condition == 'Rusak ringan')
                                    <span class="label label-info">{{$kepusdok->condition}}</span>
                                    @elseif ($kepusdok->condition == 'Rusak berat')
                                    <span class="label label-warning">{{$kepusdok->condition}}</span>
                                    @elseif ($kepusdok->condition == 'Hilang')
                                    <span class="label label-danger">{{$kepusdok->condition}}</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td>{{$kepusdok->information}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Histori alat <small>Weekly progress</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table id="datatable-responsive" class="table dt-responsive nowrap" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl dibuat</th>
                                <th>Dibuat oleh</th>
                                <th>Kondisi</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @forelse ($histories as $data)
                            <tr>
                                <td scope="row">{{$no++}}</td>
                                <td>{{$data->created_at}}</td>
                                <td>{{$data->created_by}}</td>
                                <td>{{$data->condition}}</td>
                                <td>
                                    @if ($data->information == 'Di check list oleh user')
                                    <b>{{$data->information}}</b>
                                    @else
                                    {{$data->information}}
                                    @endif

                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                                </button>
                                <strong>Tidak ada data alat!
                            </div>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection