@extends('template.index')
@section('page_title', 'Kepusdok')
@section('sub_page_title', 'Inventarisasi kepustakaan dan dokumentasi')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <strong>Success !</strong> {{session('message')}}
        </div>
        @endif
        <div class="x_panel">
            @can('kepusdok')
            <div class="x_title">
                <a href="/kepusdoks/create" class="btn btn-sm btn-dark">Tambah Inventaris</a>
            </div>
            @endcan
            
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="table-responsive">
                    <table id="datatable" class="table dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Judul</th>
                                <th>Kondisi</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @forelse ($kepusdoks as $data)
                            <tr>
                                <td scope="row">{{$no++}}</td>
                                <td>{{$data->code}}</td>
                                <td>{{$data->title}}</td>
                                <td>{{$data->condition}}</td>
                                <td class="text-center" width="340px">
                                    <a href="/kepusdoks/{{$data->id}}" class="btn btn-primary btn-sm">View </a>
                                    @can('kepusdok')
                                    @if (date('M', strtotime($data->checklist_at)) != now()->format('M') )
                                    <a href="/kepusdoks/{{$data->id}}/checks" class="btn btn-warning btn-sm">Check list </a>
                                    @endif
                                    <a href="/kepusdoks/{{$data->id}}/edit" class="btn btn-info btn-sm">Edit </a>
                                    <a href="" data-toggle="modal" data-target="#delete-{{$data->id}}"><button
                                            type="submit" class="btn btn-sm btn-danger">Delete</button></a>
                                    @endcan
                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                                </button>
                                <strong>Tidak ada data!
                            </div>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach ($kepusdoks as $data)
<div class="modal fade" tabindex="-1" role="dialog" id="delete-{{$data->id}}" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-body">
                <h4>Apakah anda akan menghapus data ini?</h4>
            </div>

            <div class="modal-footer">
                {{ Form::open(['url' => '/kepusdoks/'.$data->id, 'method'=>'DELETE']) }}
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-danger">Ya</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@endforeach

@endsection