@extends('template.index')
@section('page_title', 'Anggota')
@section('sub_page_title', 'halaman pilih admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">

                        {!!
                        Form::open(['url'=>'/admins/'.$user->id,'method'=>'PUT','enctype'=>'multipart/form-data'])
                        !!}

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                required="required" value="{{$user->name}}" readonly>
                            <div class="invalid-feedback ">
                                @error('name')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Akses</label>
                            <select name="access"
                                class="form-control @error('access') is-invalid @enderror" required>
                                <option value="administrator">Administrator</option>
                                <option value="keuangan">Bendahara</option>
                                <option value="peralatan">Peralatan</option>
                                <option value="kesra">Kesra</option>
                                <option value="kepusdok">Kepusdok</option>
                                <option value="humas">Humas</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('access')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection