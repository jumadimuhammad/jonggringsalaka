@extends('template.index')
@section('page_title', 'Admin')
@section('sub_page_title', 'List admin')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                {{-- <a href="/admins/create" class="btn btn-sm btn-dark">Tambah Admin</a> --}}
                <a href="" data-toggle="modal" data-target="#users"><button type="submit"
                        class="btn btn-sm btn-dark">Tambah Admin</button></a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('message'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span>
                    </button>
                    <strong>Success !</strong> {{session('message')}}
                </div>
                @endif
                <table class="table dt-responsive nowrap" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>No hp</th>
                            <th>Najs</th>
                            <th>Akses</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @forelse ($user as $data)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->phone}}</td>
                            <td>{{$data->najs}}</td>
                            <td>{{$data->access}}</td>
                            <td class="text-center" width="170px">
                                <a href="/users/{{$data->id}}"><button type="submit"
                                        class="btn btn-sm btn-info">Detail</button></a>
                                <a href="" data-toggle="modal" data-target="#delete-{{$data->id}}"><button type="submit"
                                        class="btn btn-sm btn-danger">Delete</button></a>
                            </td>
                        </tr>
                        @empty
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <strong>Tidak ada data anggota!
                        </div>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="users" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-body">
                <h4>Data Anggota</h4>
                <table id="datatable-responsive" class="table dt-responsive nowrap" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>No hp</th>
                            <th>Najs</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @forelse ($users as $data)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->phone}}</td>
                            <td>{{$data->najs}}</td>
                            <td class="text-center">
                                <a href="/admins/{{$data->id}}/edit"><button type="submit"
                                        class="btn btn-sm btn-warning">Tambah</button></a>
                            </td>
                        </tr>
                        @empty
                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <strong>Tidak ada data anggota!
                        </div>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@foreach ($user as $data)
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="delete-{{$data->id}}" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-body">
                <p>Apakah anda akan menghapus <strong>{{$data->name}}</strong> sebagai admin?</p>
            </div>

            <div class="modal-footer">
                {{ Form::open(['url' => '/admins/'.$data->id, 'method'=>'DELETE']) }}
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-danger">Ya</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@endforeach

@endsection