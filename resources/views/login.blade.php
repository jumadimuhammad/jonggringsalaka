<!DOCTYPE html>
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="login">
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">

                @if (session('error_login'))
                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span>
                    </button>
                    <strong>Warning!</strong> {{session('error_login')}}
                </div>
                @endif

                <section class="login_content">
                    {{ Form::open(['url' =>'/login', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                    <h1>Login Form</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" name="email" required="" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" name="password"
                            required="" />
                    </div>
                    <div>
                        <button class="btn btn-default submit">Login</button>
                        <a class="reset_pass" href="#">Lost your password?</a>
                    </div>
                    <div class="clearfix"></div>
                    {{ Form::close() }}
                </section>

            </div>
        </div>
    </div>
</body>

</html>