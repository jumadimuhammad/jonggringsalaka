@extends('template.index')
@section('page_title', 'Keuangan')
@section('sub_page_title', 'Pencatatan keuangan')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            @can('finance')
            <div class="x_title">
                <a href="/finances/create" class="btn btn-sm btn-dark">Tambah Transaksi</a>
                <div class="clearfix"></div>
            </div>
            @endcan
            
            <div class="x_content">
                @if (session('message'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span>
                    </button>
                    <strong>Success !</strong> {{session('message')}}
                </div>
                @endif
                <table id="datatable-responsive" class="table dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nama barang/Jasa</th>
                            <th>Total</th>
                            <th>Jenis transaksi</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @forelse ($finance as $data)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$data->created_at}}</td>
                            <td>{{$data->name}}</td>
                            <td>Rp. {{number_format($data->amount, 2)}}</td>
                            <td>
                                @if ($data->transaction == "debet")
                                Pemasukan
                                @else
                                Pengeluaran
                                @endif
                            </td>
                            <td class="text-center" width="170px">
                                <a href="/finances/{{$data->id}}"><button type="submit"
                                        class="btn btn-sm btn-dark">Detail</button></a>
                                @can('finance')
                                <a href="/finances/{{$data->id}}/edit"><button type="submit"
                                        class="btn btn-sm btn-warning">Edit</button></a>
                                <a href="" data-toggle="modal" data-target="#delete-{{$data->id}}"><button type="submit"
                                        class="btn btn-sm btn-danger">Delete</button></a>
                                @endcan
                            </td>
                        </tr>
                        @empty
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <strong>Tidak ada data transaksi!
                        </div>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@foreach ($finance as $data)
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="delete-{{$data->id}}" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Apakah anda akan menghapus data ini?</h4>
            </div>
            <div class="modal-footer">
                {{ Form::open(['url' => '/finances/'.$data->id, 'method'=>'DELETE']) }}
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-danger">Ya</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@endforeach

@endsection