@extends('template.index')
@section('page_title', 'Keuangan')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Design <small>different form elements</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    @if (session('message'))
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span>
                        </button>
                        <strong>Success !</strong> {{session('message')}}
                    </div>
                    @endif

                    {{ Form::open(['url' =>'/finances', 'method' => 'POST', 'enctype' => 'multipart/form-data',
                    'class' => 'form-horizontal form-label-left input_mask']) }}

                    <input type="text" value="{{ Auth::user()->id}}" name="user_id" hidden>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="">Nama Barang/Jasa</label>
                        <input type="text" class="form-control has-feedback-left"
                            class="form-control @error('name') is-invalid @enderror" required="required"
                            value="{{ old('name') }}" placeholder="Nama Barang/Jasa" name="name">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        <div class="invalid-feedback">
                            @error('name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="">Jumlah</label>
                        <input type="number" class="form-control has-feedback-right"
                            class="form-control @error('amount') is-invalid @enderror" required="required"
                            value="{{ old('amount') }}" placeholder="Jumlah" name="amount">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        <div class="invalid-feedback">
                            @error('amount')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="">Keterangan</label>
                        <input type="text" class="form-control has-feedback-left"
                            class="form-control @error('description') is-invalid @enderror" required="required"
                            value="{{ old('description') }}" placeholder="Keterangan" name="description">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        <div class="invalid-feedback">
                            @error('description')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback">
                        <label>Jenis Transaksi *:</label>
                        <p>
                            <input type="radio" class="flat" name="transaction" value="debet" checked required />
                            Pemasukan:
                            <input type="radio" class="flat" name="transaction" value="credit" />
                            Pengeluaran:
                        </p>
                    </div>

                    <br />
                    <div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback float-right">
                        <div class="fom-group mt-2 float-fight">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>
                    </div>

                    {{ Form::close() }}

                </div>
            </div>

        </div>
    </div>
</div>

@endsection