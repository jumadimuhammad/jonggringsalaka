@extends('template.index')
@section('page_title', 'Keuangan')
@section('sub_page_title', 'Some examples to get you started')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keuangan<small>Weekly progress</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Tgl dibuat</th>
                                <td>{{$finance->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{$finance->name}}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td>{{$finance->amount}}</td>
                            </tr>
                            <tr>
                                <th>Transaksi</th>
                                <td>
                                    @if ($finance->transaction == "debet")
                                    Pemasukan
                                    @else
                                    Pengeluaran
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Deskripsi</th>
                                <td>{{$finance->description}}</td>
                            </tr>
                            <tr>
                                <th>User</th>
                                <td>{{$finance->user->name}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection