@extends('template.index')
@section('page_title', 'Alat')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tooltips <small>Hover to view</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">

                        {!!
                        Form::open(['url'=>'/tools/'.$tool->id,'method'=>'PUT','enctype'=>'multipart/form-data'])
                        !!}
                        
                        <input type="text" name="user" value="{{Auth::user()->najs}}" hidden>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kode</label>
                            <input type="text" name="code" class="form-control" value="{{$tool->code}}">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Nama</label>
                            <input type="text" name="name" class="form-control" value="{{$tool->name}}">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Tahun pengadaan</label>
                            <input type="text" name="year" class="form-control" value="{{$tool->year}}"
                                placeholder="2021">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Ciri-ciri</label>
                            <input type="text" name="mark" class="form-control" value="{{$tool->mark}}">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kondisi</label>
                            <select id="" name="condition" class="form-control" required>
                                <option value="{{$tool->condition}}">{{$tool->condition}}</option>
                                <option value="Baik">Baik</option>
                                <option value="Rusak">Rusak</option>
                                <option value="Hilang">Hilang</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kategori</label>
                            <select id="" name="category" class="form-control" required>
                                <option value="{{$tool->category}}">{{$tool->category}}</option>
                                <option value="Logam">Logam</option>
                                <option value="Non logam">Non logam</option>
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Keterangan</label>
                            <input type="text" name="information" class="form-control" value="{{$tool->information}}">
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection