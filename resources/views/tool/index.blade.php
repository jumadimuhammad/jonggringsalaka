@extends('template.index')
@section('page_title', 'Peralatan')
@section('sub_page_title', 'Inventarisasi peralatan')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <strong>Success !</strong> {{session('message')}}
        </div>
        @endif
        <div class="x_panel">
            @can('tool')
            <div class="x_title">
                <a href="/tools/create" class="btn btn-sm btn-dark">Tambah Alat</a>
            </div>
            @endcan
            
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="table-responsive">
                    <table id="datatable" class="table dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode alat</th>
                                <th>Nama</th>
                                <th>Kondisi</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @forelse ($tools as $data)
                            <tr>
                                <td scope="row">{{$no++}}</td>
                                <td>{{$data->code}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->condition}}</td>
                                <td class="text-center" width="340px">
                                    <a href="/tools/{{$data->id}}" class="btn btn-primary btn-sm">View </a>
                                    @can('tool')
                                    @if (date('M', strtotime($data->checklist_at)) != now()->format('M') )
                                    <a href="/tools/{{$data->id}}/checks" class="btn btn-warning btn-sm">Check list </a>
                                    @endif
                                    <a href="/tools/{{$data->id}}/edit" class="btn btn-info btn-sm">Edit </a>
                                    <a href="" data-toggle="modal" data-target="#delete-{{$data->id}}"><button
                                            type="submit" class="btn btn-sm btn-danger">Delete</button></a>
                                    @endcan
                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                                </button>
                                <strong>Tidak ada data alat!
                            </div>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach ($tools as $data)
<div class="modal fade" tabindex="-1" role="dialog" id="delete-{{$data->id}}" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-body">
                <h4>Apakah anda akan menghapus data ini?</h4>
            </div>

            <div class="modal-footer">
                {{ Form::open(['url' => '/tools/'.$data->id, 'method'=>'DELETE']) }}
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-danger">Ya</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@endforeach

@endsection