@extends('template.index')
@section('page_title', 'Kategori')
@section('sub_page_title', 'Some examples to get you started')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Kategori<small>Weekly progress</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Tgl dibuat</th>
                                <td>{{$category->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Kategori</th>
                                <td>{{$category->name}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection