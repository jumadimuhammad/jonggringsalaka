@extends('template.index')
@section('page_title', 'Kategori')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Design <small>different form elements</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    @if (session('message'))
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span>
                        </button>
                        <strong>Success !</strong> {{session('message')}}
                    </div>
                    @endif

                    {{ Form::open(['url' =>'/categories', 'method' => 'POST', 'enctype' => 'multipart/form-data',
                    'class' => 'form-horizontal form-label-left input_mask']) }}

                    <input type="text" value="{{ Auth::user()->id}}" name="user_id" hidden>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <label for="">Nama Kategori</label>
                        <input type="text" class="form-control has-feedback-left"
                            class="form-control @error('name') is-invalid @enderror" required="required"
                            value="{{ old('name') }}" placeholder="Kategori" name="name">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        <div class="invalid-feedback">
                            @error('name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <br />
                    <div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback float-right">
                        <div class="fom-group mt-2 float-fight">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>
                    </div>

                    {{ Form::close() }}

                </div>
            </div>

        </div>
    </div>
</div>

@endsection