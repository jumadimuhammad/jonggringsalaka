@extends('template.index')
@section('page_title', 'Anggota')
@section('sub_page_title', 'Daftar anggota')

@section('content')
<div class="page-title">
    <div class="title_left">
        <h3>
            @can('kepusdok')
                <a href="/users/create" class="btn btn-sm btn-dark">Tambah Anggota</a>
            @endcan
        </h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5   form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <!-- @can('kepusdok')
            <div class="x_title">
                <a href="/users/create" class="btn btn-sm btn-dark">Tambah Anggota</a>
                <div class="clearfix"></div>
            </div>
            @endcan -->

            <div class="x_content">
                @if (session('message'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span>
                    </button>
                    <strong>Success !</strong> {{session('message')}}
                </div>
                @endif
                <!-- <table id="datatable-responsive" class="table dt-responsive nowrap" cellspacing="0"> -->
                <table class="table dt-responsive nowrap table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th>No hp</th>
                            <th>Najs</th>
                            <th class="text-center" style="width:190px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @forelse ($users as $data)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->email}}</td>
                            <td>{{$data->address}}</td>
                            <td>{{$data->phone}}</td>
                            <td>{{$data->najs}}</td>
                            <td class="text-center" width="170px">
                                <a href="/users/{{$data->id}}"><button type="submit"
                                        class="btn btn-sm btn-info">Detail</button></a>
                                @can('kepusdok')
                                <a href="/users/{{$data->id}}/edit"><button type="submit"
                                        class="btn btn-sm btn-warning">Edit</button></a>
                                <a href="" data-toggle="modal" data-target="#delete-{{$data->id}}"><button type="submit"
                                        class="btn btn-sm btn-danger">Delete</button></a>
                                @endcan
                            </td>
                        </tr>
                        @empty
                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <strong>Tidak ada data anggota!
                        </div>
                        @endforelse
                    </tbody>
                </table>
                <div class="navbar-right">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@foreach ($users as $data)
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="delete-{{$data->id}}" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-body">
                <h4>Apakah anda akan menghapus data ini?</h4>
            </div>

            <div class="modal-footer">
                {{ Form::open(['url' => '/users/'.$data->id, 'method'=>'DELETE']) }}
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-danger">Ya</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@endforeach

@endsection