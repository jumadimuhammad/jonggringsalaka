@extends('template.index')
@section('page_title', 'Profil')
@section('sub_page_title', 'Explore your self')

@section('content')

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
                    <div class="profile_img col-md-5 col-sm-5 col-xs-12">
                        <div id="crop-avatar">
                            <!-- Current avatar -->
                            <img class="img-responsive avatar-view" src="{{asset('img_member')}}/profile.png" alt="Avatar"
                                title="Change the avatar">
                        </div>
                    </div>
                    <h3>{{Auth::user()->name}}</h3>

                    <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> {{Auth::user()->address}}
                        </li>

                        <li>
                            <i class="fa fa-briefcase user-profile-icon"></i> {{Auth::user()->najs}}
                        </li>

                        <li class="m-top-xs">
                            <i class="fa fa-external-link user-profile-icon"></i>
                            {{Auth::user()->email}}
                        </li>
                    </ul>
                    <br />
                    <br />
                </div>
                
                <div class="x_title">
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 profile_left">
                    <ul class="list-unstyled user_data">
                        <h4>Nama panggilan</h4>
                        <li>
                            <i class="fa fa-check-circle-o"></i>  {{Auth::user()->other_name}}
                        </li>

                        <h4>No handphone</h4>
                        <li>
                            <i class="fa fa-check-circle-o"></i>  {{Auth::user()->phone}}
                        </li>

                        <h4>Tahun diklat</h4>
                        <li>
                            <i class="fa fa-check-circle-o"></i>  {{Auth::user()->year}}
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 profile_left">
                    <ul class="list-unstyled user_data">
                        <h6>Provinsi</h6>
                        <li>
                            <i class="fa fa-check-circle-o"></i>  {{Auth::user()->province}}
                        </li>

                        <h6>Kabupaten/kota</h6>
                        <li>
                            <i class="fa fa-check-circle-o"></i>  {{Auth::user()->city}}
                        </li>

                        <h6>Kecamatan</h6>
                        <li>
                            <i class="fa fa-check-circle-o"></i>  {{Auth::user()->district}}
                        </li>

                        <h6>Kelurahan/desa</h6>
                        <li>
                            <i class="fa fa-check-circle-o"></i>  {{Auth::user()->village}}
                        </li>
                    </ul>
                </div>
                <a href="/users/{{Auth::user()->id}}/edit" class="btn btn-info navbar-right"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>

            </div>
        </div>
    </div>
</div>

@endsection