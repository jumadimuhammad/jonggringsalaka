@extends('template.index')
@section('page_title', 'Anggota')
@section('sub_page_title', 'halaman perubahan data anggota')

@section('content')
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">

                        {!!
                        Form::open(['url'=>'/users/'.$user->id,'method'=>'PUT','enctype'=>'multipart/form-data'])
                        !!}

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                required="required" value="{{$user->name}}">
                            <div class="invalid-feedback ">
                                @error('name')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Nama Lapangan</label>
                            <input type="text" name="other_name" class="form-control" value="{{$user->other_name}}" required="required">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">No Hp</label>
                            <input type="text" name="phone" class="form-control" value="{{$user->phone}}" required="required">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Email</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                required="required" value="{{$user->email}}" readonly>
                            <div class="invalid-feedback">
                                @error('email')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Najs</label>
                            <input type="text" name="najs" class="form-control @error('najs') is-invalid @enderror"
                                required="required" value="{{$user->najs}}">
                            <div class="invalid-feedback">
                                @error('najs')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Tahun Angkatan</label>
                            <input type="text" name="year" class="form-control" value="{{$user->year}}" required="required">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Provinsi</label>
                            <select id="province" class="form-control @error('province') is-invalid @enderror" required>
                                <option value="{{$user->province}}">{{$user->province}}</option>
                                @foreach ($provinces as $data)
                                <option value="{{$data->kode_bps}}">{{$data->nama_bps}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                @error('province')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <input type="text" id="province_value" name="province"  required="required" value="{{$user->province}}" hidden>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kota/Kabupaten</label>
                            <select id="city" class="form-control @error('city') is-invalid @enderror" required>
                                <option value="{{$user->city}}">{{$user->city}}</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('city')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <input type="text" id="city_value" name="city" required="required" value="{{$user->city}}" hidden>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kecamatan</label>
                            <select id="district" class="form-control @error('district') is-invalid @enderror" required>
                                <option value="{{$user->district}}">{{$user->district}}</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('district')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <input type="text" id="district_value" name="district" required="required" value="{{$user->district}}" hidden>                        

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Kelurahan/Desa</label>
                            <select id="village" name="village"
                                class="form-control @error('village') is-invalid @enderror" required>
                                <option value="{{$user->village}}">{{$user->village}}</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('village')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Alamat Lengkap</label>
                            <input type="text" name="address"
                                class="form-control @error('address') is-invalid @enderror" value="{{$user->address}}" required="required">
                            <div class="invalid-feedback">
                                @error('address')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const province = document.querySelector('#province');
    const province_value = document.querySelector('#province_value');
    const city = document.querySelector('#city');
    const city_value = document.querySelector('#city_value');
    const district = document.querySelector('#district');
    const district_value = document.querySelector('#district_value');
    const village = document.querySelector('#village');
    const village_value = document.querySelector('#village_value');

    
    province.addEventListener('change', async function(){
        let ct = []
        city.innerHTML = '<option value="">Pilih kota/kabupaten</option>'
        province_value.value = province.options[province.selectedIndex].text

        await fetch('/cities/' + province.value)
        .then(response => response.json())
        .then(data => ct = data.cities)

        for (let i in ct) {
             city.innerHTML += '<option value="' + ct[i].kode_bps + '">' + ct[i].nama_bps + '</option>'
        }
    });

    city.addEventListener('change', async function(){
        let dt = []
        district.innerHTML = '<option value="">Pilih kecamatan</option>'
        city_value.value = city.options[city.selectedIndex].text

        await fetch('/districts/' + city.value)
        .then(response => response.json())
        .then(data => dt = data.districts)

        for (let i in dt) {
             district.innerHTML += '<option value="' + dt[i].kode_bps + '">' + dt[i].nama_bps + '</option>'
        }
    });

    district.addEventListener('change', async function(){
        let vl = []
        village.innerHTML = '<option value="">Pilih kelurahan/desa</option>'
        district_value.value = district.options[district.selectedIndex].text

        await fetch('/villages/' + district.value)
        .then(response => response.json())
        .then(data => vl = data.villages)

        for (let i in vl) {
             village.innerHTML += '<option value="' + vl[i].nama_bps + '">' + vl[i].nama_bps + '</option>'
        }
    });

</script>

@endsection