@extends('template.index')
@section('page_title', 'Anggota')
@section('sub_page_title', 'halaman detail anggota')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Nama</th>
                                <td>{{$user->name}}</td>
                            </tr>
                            <tr>
                                <th>Nama panggilan</th>
                                <td>{{$user->other_name}}</td>
                            </tr>
                            <tr>
                                <th>No hp</th>
                                <td>{{$user->phone}}</td>
                            </tr>
                            <tr>
                                <th>Tahun diklat</th>
                                <td>{{$user->year}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <th>Najs</th>
                                <td><h4><span class="label label-success">{{$user->najs}}</span></h4></td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>{{$user->address}}</td>
                            </tr>
                            <tr>
                                <th>Provinsi</th>
                                <td>{{$user->province}}</td>
                            </tr>
                            <tr>
                                <th>Kota/kabupaten</th>
                                <td>{{$user->city}}</td>
                            </tr>
                            <tr>
                                <th>Kecamatan</th>
                                <td>{{$user->district}}</td>
                            </tr>
                            <tr>
                                <th>Kelurahan/desa</th>
                                <td>{{$user->village}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection