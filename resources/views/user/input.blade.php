@extends('template.index')
@section('page_title', 'Anggota')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tooltips <small>Hover to view</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        @if (session('message'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <strong>Success !</strong> {{session('message')}}
                        </div>
                        @endif

                        {{ Form::open(['url' =>'/users', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Nama Lengkap *</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                required="required" value="{{ old('name') }}" placeholder="Masukkan nama lengakap">
                            <div class="invalid-feedback ">
                                @error('name')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Nama Lapangan</label>
                            <input type="text" name="other_name" class="form-control" value=" {{ old('other_name') }}" placeholder="Masukkan nama lapangan">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">No Hp</label>
                            <input type="text" name="phone" class="form-control" value=" {{ old('phone') }}" placeholder="Masukkan no handphone">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Email *</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                required="required" value="{{ old('email') }}" placeholder="Masukkan email">
                            <div class="invalid-feedback">
                                @error('email')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Najs *</label>
                            <input type="text" name="najs" class="form-control @error('najs') is-invalid @enderror"
                                required="required" value="{{ old('najs') }}" placeholder="Masukkan najs">
                            <div class="invalid-feedback">
                                @error('najs')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <label for="">Tahun Diklat</label>
                            <input type="text" name="year" class="form-control" value=" {{ old('year') }}" placeholder="Masukkan tahun diklat">
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Alamat Lengkap</label>
                            <input type="text" name="address"
                                class="form-control @error('address') is-invalid @enderror"
                                value=" {{ old('address') }}" placeholder="Masukkan alamat lengakap">
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection