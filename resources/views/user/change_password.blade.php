@extends('template.index')
@section('page_title', 'Anggota')
@section('sub_page_title', 'halaman ganti password')

@section('content')
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::open(['url' =>'/users/'.Auth::user()->id.'/change-password', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) }}

                        @if (session('message'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <strong>Gagal !</strong> {{session('message')}}
                        </div>
                        @endif

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                            <label for="">Password Lama</label>
                            <input type="password" name="old_password" class="form-control" required placeholder="Masukkan password lama"/>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                            <label for="">Password Baru</label>
                            <input type="password" name="new_password" class="form-control" required placeholder="Masukkan password baru"/>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                            <label for="">Konfirmasi Password Baru</label>
                            <input type="password" name="confirm_password" class="form-control" required placeholder="Konfirmasi password baru"/>
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection