<!DOCTYPE html>
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="/" class="site_title">
                            <strong>Jonggring Salaka</strong></a>
                    </div>
                    <div class="clearfix"></div>
                    <hr />

                    <!-- sidebar menu -->
                    @include('template.aside')
                    <!-- /sidebar menu -->

                </div>
            </div>

            <!-- top navigation -->
            @include('template.nav')
            <!-- /top navigation -->

            <!-- page content -->

            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>@yield('page_title') <small>@yield('sub_page_title')</small></h3>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    @yield('content')
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    <h4>Sistem informasi jonggring salaka</h4> :: Develop with <i class="fa fa-coffee"></i> <i class="fa fa-heart"></i> ::: 133318
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    @include('template.js')

</body>

</html>