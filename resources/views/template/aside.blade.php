<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ul>
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a href="/users"><i class="fa fa-group"></i> Anggota</a></li>
            <li><a href="/finances"><i class="fa fa-dollar"></i> Keuangan</a></li>
            <li><a href="/tools"><i class="fa fa fa-gears"></i> Peralatan</a></li>
            <li><a href="/kesras"><i class="fa fa fa-book"></i> Kesra</a></li>
            <li><a href="/kepusdoks"><i class="fa fa fa-archive"></i> Kepusdok</a></li>
            
            @can('administrator')
            <li><a href="/admins"><i class="fa fa fa-user"></i> Admin</a></li>
            @endcan
            
            @can('humas')
            <li><a href="/posts"><i class="fa fa fa-newspaper-o"></i> Post</a></li>
            <li><a href="/categories"><i class="fa fa fa-tag"></i> Kategori</a></li>
            @endcan
        </ul>
    </div>

</div>