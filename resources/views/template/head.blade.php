<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Jonggring Salaka | @yield('page_title')</title>
{{-- <link rel="shortcut icon" href="{{asset('img')}}/icon.png"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('css')}}/trix.css">

<!-- Bootstrap -->
<link href="{{asset('css')}}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{asset('css')}}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
{{--
<link href="{{asset('css')}}/font-awesome/css/all.css" rel="stylesheet"> --}}

<!-- Datatables -->
<link href="{{asset('css')}}/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="{{asset('css')}}/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="{{asset('css')}}/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="{{asset('css')}}/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="{{asset('css')}}/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="{{asset('css')}}/custom.min.css" rel="stylesheet">