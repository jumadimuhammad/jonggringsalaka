<!-- jQuery -->
<script src="{{asset('js')}}/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="{{asset('js')}}/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Datatables -->
<script src="{{asset('js')}}/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('js')}}/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<!-- Custom Theme Scripts -->
<script src="{{asset('js')}}/trix.js"></script>
<script src="{{asset('js')}}/custom.min.js"></script>