@extends('template.index')
@section('page_title', 'Dashboard')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <h2 class="text-capitalize">{{$post->title}}</h2>
            <span class="byline">{{$post->created_at->diffForHumans()}} by {{$post->user->name}}</span>
            <p>{!!$post->body!!}</p>
        </div>
    </div>
</div>
@endsection