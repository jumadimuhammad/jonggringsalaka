@extends('template.index')
@section('page_title', 'Dashboard')
@section('sub_page_title', 'Some informations to get you started')

@section('content')
<div class="row top_tiles">
    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-users"></i></div>
            <div class="count">{{$count}}</div>
            <h3>Anggota</h3>
            <p>Total anggota terdaftar</p>
        </div>
    </div>
    <!-- <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-comments-o"></i></div>
            <div class="count">179</div>
            <h3>New Sign ups</h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
            <div class="count">179</div>
            <h3>New Sign ups</h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-check-square-o"></i></div>
            <div class="count">179</div>
            <h3>New Sign ups</h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div> -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <h1 id="glyphicons" class="page-header">Informasi</h1>

            <ul class="list-unstyled timeline">
                @forelse ($posts as $data)
                <li>
                    <div class="block">
                        <div class="tags">
                            <a href="" class="tag">
                                <span>{{strtoupper($data->category->name)}}</span>
                            </a>
                        </div>
                        <div class="block_content">
                            <h2 class="title text-capitalize">
                                <a href="/dashboard/{{$data->slug}}"> {{$data->title}} </a>
                            </h2>
                            <div class="byline">
                                    <span>{{$data->created_at->diffForHumans()}}</span> by {{$data->user->name}}
                        </div>
                        <p class="excerpt">{!!Str::limit($data->body, 200)!!} … <a href="/dashboard/{{$data->slug}}">Read&nbsp;More</a>
                        </p>
                    </div>
        </div>
        </li>
        @empty
        <p>Tidak ada informasi</p>
        @endforelse
        </ul>
    </div>
</div>
</div>
@endsection