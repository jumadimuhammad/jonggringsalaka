@extends('template.index')
@section('page_title', 'Post')
@section('sub_page_title', 'Some examples to get you started')

@section('content')
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tooltips <small>Hover to view</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">

                        {!!
                        Form::open(['url'=>'/posts/'.$post->slug,'method'=>'PUT','enctype'=>'multipart/form-data'])
                        !!}

                        <input type="text" name="user_id" value="{{Auth::user()->id}}" hidden>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="title">Title</label>
                            <input type="text" id='title' name="title"
                                class="form-control @error('title') is-invalid @enderror" value="{{$post->title}}"
                                autofocus>
                            <div class="invalid-feedback">
                                @error('title')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="slug">Slug</label>
                            <input type="text" id='slug' name="slug"
                                class="form-control @error('slug') is-invalid @enderror" placeholder="slug"
                                value="{{$post->slug}}" readonly required>
                            <div class="invalid-feedback">
                                @error('slug')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Category</label>
                            <select id="" name="category_id"
                                class="form-control @error('category_id') is-invalid @enderror" required>
                                @foreach ($categories as $data)
                                @if ( $data->id == $post->category_id)
                                <option value="{{$data->id}}" selected>{{$data->name}}</option>
                                @else
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                @error('category_id')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label for="">Body</label>
                            <input id="body" type="hidden" name="body" value="{{$post->body}}">
                            <div class="invalid-feedback">
                                @error('body')
                                {{ $message }}
                                @enderror
                            </div>
                            <trix-editor input="body"></trix-editor>
                        </div>

                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <button class="btn btn-dark" type="submit">Simpan</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const title = document.querySelector('#title');
    const slug = document.querySelector('#slug');

    title.addEventListener('change', function(){
        fetch('/posts/check-slug?title=' + title.value)
        .then(response => response.json())
        .then(data => slug.value = data.slug)
    });
</script>

@endsection