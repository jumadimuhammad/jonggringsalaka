@extends('template.index')
@section('page_title', 'Post')
@section('sub_page_title', 'halaman admin post')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <a href="/posts/create" class="btn btn-sm btn-dark">Tambah Post</a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('message'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span>
                    </button>
                    <strong>Success !</strong> {{session('message')}}
                </div>
                @endif
                <table id="datatable-responsive" class="table dt-responsive nowrap" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Slug</th>
                            <th>Kategori</th>
                            <th>Body</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @forelse ($posts as $data)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$data->title}}</td>
                            <td>{{$data->slug}}</td>
                            <td>{{$data->category->name}}</td>
                            <td>{!!Str::limit($data->body, 50)!!}</td>
                            <td class="text-center" width="170px">
                                <a href="/posts/{{$data->slug}}"><button type="submit"
                                        class="btn btn-sm btn-dark">Detail</button></a>
                                <a href="/posts/{{$data->slug}}/edit"><button type="submit"
                                        class="btn btn-sm btn-warning">Edit</button></a>
                                <a href="" data-toggle="modal" data-target="#delete-{{$data->slug}}"><button type="submit"
                                        class="btn btn-sm btn-danger">Delete</button></a>
                            </td>
                        </tr>
                        @empty
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <strong>Tidak ada data post!
                        </div>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@foreach ($posts as $data)
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="delete-{{$data->slug}}" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-body">
                <h4>Apakah anda akan menghapus data ini?</h4>
            </div>

            <div class="modal-footer">
                {{ Form::open(['url' => '/posts/'.$data->slug, 'method'=>'DELETE']) }}
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-danger">Ya</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@endforeach

@endsection